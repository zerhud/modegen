Modegen is a codegeneration framework. you can write a code template using jinja2
and it will generate code from it and embadded IDL (Interface Definition Language)
and from some extra settings (you can provide it as json or as info file).
Generator creates data tree with all data you provied, after that jinja can use
it for generate code.

**How to Compile?**

The best way to compile is using [nix](https://nixos.org/nix/). Also this project
is under cmake. so you can compile it if you have
*  cmake
*  boost
*  [pybind11](https://github.com/pybind/pybind11) (header-only library)
*  python3 (it has bug with double init, so you will see warning at each generation, it fixed with nix)
*  jinja2
*  modern c++ compiller (c++17 support)

[nix](https://nixos.org/nix/) is package manager with declarative language.
for compile it using nix just cd to nix directory and run nix-shell.
(the nix must be installed, for install it use instruction from
[this](https://nixos.org/nix/) page.)
it will build environment for you. after that you can create build dir and compile it.
all commands:
`cd nix; nix-shell;` it starts bash with environment contains all for compilation
inside it type `mkdir ../build; cd ../build; cmake .. -G Ninja`

**How to Use?**

example of IDL you can find in tests/interface/mod{1,2} files.
just run from build directory
`./modegen -Iinterface=../tests/interface/mod1 -g declarations --outdir tmp`
or run
`./modegen -h`
to see help message

The declarations is declarations.info file in generation/interface/cpp.
it contains all data needed for generate interface code.

**How to write own template?**

template is set of jinja files, and info or json file with rules to generate it.
for now there is only two generators: cpp and cmake. c, python, browser js are
comming :)
example of info file you can find in `generation/interface/cpp/declarations.info`
rule manings
*  `defaults`: will be passed to each generator if it has no such item.
*  `gen`: parts of generation, resulting files
*  `[part_name]`: inner name of part (file) for generate
    *  each part must contains `parser` info: wich parser must be used for it
    *  `target` is generator that will be used for generate target
    *  `naming` all names will be converted to it, empty: pass as is
    *  `input` input jinja template
    *  `output` output file name (will be appened to the `--outdir` sh command)
    *  `inc_part` will include other part
    *  `inc` includes a file
    *  `filter` filters data: only interface, for example, will be passed to template
    *  `forwards` passes python script to generator. `ex` may be called from template
*  `cmake` target needs in `libraries` definitions

after you create an info file, you can pass it as `-g` parameter and get the result
